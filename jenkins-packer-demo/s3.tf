resource "aws_s3_bucket" "terraform-state" {
    bucket = "terraform-state-sncloud"
    acl = "private"

    tags {
        Name = "Terraform state"
    }
}
