terraform {
  backend "s3" {
    bucket = "terraform-state-a2b6219-sn"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}
